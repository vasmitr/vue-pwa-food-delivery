const path = require('path')
const fs = require('fs')
const express = require('express')
const { createBundleRenderer } = require('vue-server-renderer')
const serverBundle = require('./dist/vue-ssr-server-bundle.json')
const clientManifest = require('./dist/vue-ssr-client-manifest.json')

const renderer = createBundleRenderer(serverBundle, {
  runInNewContext: false, // рекомендуется
  template: require('fs').readFileSync('./dist/index.html', 'utf-8'),
  clientManifest,
})

const server = express()

server.use('/static', express.static('dist/static'))

server.get('/service-worker.js', (req, res) => {
  res.setHeader("Content-Type", 'text/javascript');
  res.write(fs.readFileSync(path.join(__dirname + '/dist/service-worker.js')))
  res.end()
})

server.get('/custom-service-worker.js', (req, res) => {
  res.setHeader("Content-Type", 'text/javascript');
  res.write(fs.readFileSync(path.join(__dirname + '/build/custom-service-worker.js')))
  res.end()
})


server.get('*', (req, res) => {
  const context = {url: req.url}
    res.setHeader("Content-Type", 'text/html');
    renderer.renderToString(context, (err, html) => {
      if (err) {
        console.log(err)
        res.end('<h1>500 Server Error</h1>')
      }
      res.end(html)
  })
})

server.listen(8080)

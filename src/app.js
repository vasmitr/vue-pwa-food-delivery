import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import App from './App.vue'
import { createRouter } from './router'
import { createStore } from './store'
import { sync } from 'vuex-router-sync'

Vue.use(Vuetify)

export function createApp () {
  const router = createRouter()
  const store = createStore()

  const unsync = sync(store, router)

  const app = new Vue({
    router,
    store,
    render: h => h(App),
    created () {
      unsync()
    }
  })

  return { app, router, store }
}

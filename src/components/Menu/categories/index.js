import Categories from './Categories'
import CategoryDetail from './CategoryDetail'

export { Categories, CategoryDetail }

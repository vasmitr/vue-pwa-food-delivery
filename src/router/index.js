import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export function createRouter () {
  return new Router({
    mode: 'history',
    routes: [
      {
        path: '/menu',
        name: 'Categories',
        component: () => import('@/components/Menu/categories/Categories.vue')
      },
      {
        path: '/',
        name: 'Home',
        redirect: '/menu'
      },
      {
        path: '/categories',
        redirect: '/menu'
      },
      {
        path: '/menu/categories',
        redirect: '/menu'
      },
      {
        path: '/menu/category/:name',
        component: () => import('@/components/Menu/categories/CategoryDetail.vue'),
        name: 'CategoryDetail'
      },
      {
        path: '/menu/product/:product_id',
        name: 'ProductDetail',
        component: () => import('@/components/Menu/products/ProductDetail')
      },
      {
        path: '*',
        component: () => import('@/components/AppShell/NotFound.vue')
      }
    ]
  })
}

import * as firebase from 'firebase'

// Initialize Firebase
const config = {
  apiKey: 'AIzaSyCd3YbKO9AtzxdCjIgPT22PLoQKhdwDDNw',
  authDomain: 'pwa-food-delivery.firebaseapp.com',
  databaseURL: 'https://pwa-food-delivery.firebaseio.com',
  projectId: 'pwa-food-delivery',
  storageBucket: 'pwa-food-delivery.appspot.com',
  messagingSenderId: '815053071286'
}

firebase.initializeApp(config)
const db = firebase.database()

// let db
// console.log(process.env.NODE_ENV)
// if (typeof window !== 'undefined') {
//   firebase.initializeApp(config)
//   db = firebase.database()
// } else if (process.env.NODE_ENV !== 'development' || process.env.NODE_ENV !== 'production') {
//   const admin = require('firebase-admin')
//   const serviceAccount = require('./pwa-food-delivery-firebase-adminsdk-1571d-e010cfd551.json')
//
//   admin.initializeApp({
//     credential: admin.credential.cert(serviceAccount),
//     databaseURL: 'https://pwa-food-delivery.firebaseio.com'
//   })
//
//   db = admin.database()
// }

export default db

import db from '@/firebase/index'
import idbKeyval from 'idb-keyval'

const fetchFromServer = () => {
  return db.ref('cities/cityInfo')
    .once('value', res => {
      console.log('[*] fetched from firebase')
      return res.val()
    })
}

export function fetchCityInfoService () {
  if (typeof window !== 'undefined' && 'indexedDB' in window) {
    return idbKeyval.get('cityInfo')
      .then(val => {
        // if cityInfo exists, put it into db, else fetch from firebase
        if (val) {
          console.log('[*] fetched from indexedDB', val)
          return val
        } else {
          return fetchFromServer()
            .then((data) => idbKeyval.set('cityInfo', data))
        }
      })
  } else {
    return fetchFromServer()
  }
}

import Vuex from 'vuex'
import Vue from 'vue'
import { fetchCityInfoService } from '../services/fetchCityInfoService'

Vue.use(Vuex)

export function createStore () {
  return new Vuex.Store({
    state: {
      cityInfo: {
        menu: {
          categories: []
        }
      }
    },
    getters: {
      categories: (state) => state.cityInfo.menu.categories,
      category: (state) => (name) => state.cityInfo.menu.categories.find((category) => category.name === name),
      products: (state) => [].concat.apply([], state.cityInfo.menu.categories.map((category) => category.products)),
      product: (state, getters) => (id) => getters.products.find((product) => product.id.toString() === id.toString())
    },
    mutations: {
      setCityInfo (state, cityInfo) {
        state.cityInfo = cityInfo
      }
    },
    actions: {
      fetchCityInfo (context) {
        return fetchCityInfoService().then((data) => {
          // On server data is snapshot :/
          if (typeof window === 'undefined') {
            data = data.val()
          }
          context.commit('setCityInfo', data)
        })
      }
    }
  })
}
